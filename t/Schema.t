use strict;
use warnings;

use Test::More;
use Test::Warnings;

use_ok('Demo::PetStore::Schema');
use_ok('Demo::PetStore::Schema::Result::Pet');

my $db  = 'share/demo-petstore-schema.db';
my $dsn = sprintf 'dbi:SQLite:dbname=%s', $db;
my @connect = ( $dsn );
my $s = Demo::PetStore::Schema->connect(@connect);

subtest 'save/get pet rows' => sub {
    my $rs;
    $rs = $s->resultset('Pet');
    my $initial_count = $rs->count;

    $rs->create({
        name => 'Fido',
    });

    my $final_count = $rs->count;
    cmp_ok($final_count, '>', $initial_count,
            'Seem to have added at least one row');

};

done_testing;
