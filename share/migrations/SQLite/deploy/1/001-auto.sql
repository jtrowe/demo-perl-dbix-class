--
-- Created by SQL::Translator::Producer::SQLite
-- Created on Sat Mar  2 10:02:47 2024
--

;
BEGIN TRANSACTION;
--
-- Table: "pet"
--
CREATE TABLE "pet" (
  "pet_id" INTEGER PRIMARY KEY NOT NULL,
  "name" varchar(255) NOT NULL
);
COMMIT;
