# Demo-Perl-DBIx-Class

Demonstration Perl DBIx::Class code for reuse in demonstration
applications.


## Database Migration Notes

Show version of schema.

```sh
carton exec -- local/bin/dbic-migration --schema_class=Demo::PetStore::Schema -I lib status
```


Prepares a migration for SQLite for current version of schema.

```sh
carton exec -- local/bin/dbic-migration --schema_class=Demo::PetStore::Schema -I lib --database SQLite prepare
```


Installs a migration for SQLite for current version of schema.

```sh
carton exec -- local/bin/dbic-migration --schema_class=Demo::PetStore::Schema -I lib --database SQLite install
```

